# Contributing

## Adding a new repository

* Add/Remove/Change only one line in `list.json`.
* Do not edit any other file.
* Create a Pull Request and prefix the title with `add:`, `remove:`, `change:`.

