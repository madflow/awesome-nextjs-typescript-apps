# Awesome NextJs using Typescript apps

> An awesome list of real-world open source app repositories - using NextJs with Typescript. No demos and xyz clones.



- [growi](https://github.com/weseek/growi) - :anchor: GROWI - Team collaboration software using markdown - 1088  ⭐ - MIT License
  

- [linen.dev](https://github.com/Linen-dev/linen.dev) - Google-searchable Slack alternative for Communities - 902  ⭐ - GNU Affero General Public License v3.0
  

- [metlo](https://github.com/metlo-labs/metlo) - Metlo is an open-source API security platform. - 620  ⭐ - MIT License
  

- [letterpad](https://github.com/letterpad/letterpad) - Letterpad is an open-source and a high performant publishing engine, built with react &amp; graphql and runs ridiculously fast 🚀 - 563  ⭐ - MIT License
  

- [lenster](https://github.com/lensterxyz/lenster) - Lenster is a decentralized, and permissionless social media app built with Lens Protocol 🌿  - 350  ⭐ - MIT License
  


